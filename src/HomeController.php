<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class HomeController extends Controller
{
    /**
     * @var Client
     * recebe instância a classe GuzzleHttp
     */
    private $client;
    /**
     * @var string
     * armazena endereço para requisição
     */
    private $url;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->url = 'http://www.guiatrabalhista.com.br/guia/salario_minimo.htm';
    }

    public function estracta()
    {
        /**
         * request => Requisita página web
         * getContents => Seleciona os dados de body da requisição
         */
        $request    = $this->client->request('GET', $this->url);
        $html       = $request->getBody()->getContents();

        /**
         * clearHtml => Limpa array primário de entrada
         * mountOutput => Monta array secundário de saída
         */
        $elementsOutput = $this->mountOutput($this->clearHtml($html));

        /**
         * Saída do array de dados tratados
         */
        dd($elementsOutput);
    }

    /**
     * @param $html
     * @return array
     * função de limpeza do array de leitura
     */
    public function clearHtml($html)
    {
        $start  = strpos($html,'<tbody',0); //localiza posição inicio da tabela
        $html   = substr($html,$start); //descarta dados anteriores a tabela
        $stop   = strpos($html,'</tbody>',0); //localiza posição fim da tabela
        $html   = substr($html,0,$stop); //descarta dados posteriores da tabela

        $html   = str_replace('<p style="margin: 0px; padding: 0px;">', '', $html); //retira tags '<p>'
        $html   = str_replace('</p>', '', $html); //retira tags '</p>

        $crawler    = new Crawler($html); //instância classe de leitura de tags
        $arrayClear = $crawler->filterXPath("//td")->extract(['_text']); //filtra tags 'td'

        unset($arrayClear[0], $arrayClear[1], $arrayClear[2], $arrayClear[3], $arrayClear [4], $arrayClear[5]); //limpa cabeçalho

        return $arrayClear;
    }

    /**
     * @param $elementos
     * @return array
     * função de montagem de array de saída
     */
    public function mountOutput($elements)
    {
        $count  = 0;
        $result = [];

        do{ //incrementa array de saída com dados de leitura
            $result[$count]['vigencia']     = preg_replace('/\s/',' ',ltrim(array_shift($elements)));
            $result[$count]['valor_mensal'] = preg_replace('/\s/',' ',ltrim(array_shift($elements)));
            $result[$count]['valor_diario'] = preg_replace('/\s/',' ',ltrim(array_shift($elements)));
            $result[$count]['valor_hora']   = preg_replace('/\s/',' ',ltrim(array_shift($elements)));
            $result[$count]['norma_legal']  = preg_replace('/\s/',' ',ltrim(array_shift($elements)));
            $result[$count]['d_o_u']        = preg_replace('/\s/',' ',ltrim(array_shift($elements)));
            $count++;

        }while($elements);

        return $result;
    }
}
